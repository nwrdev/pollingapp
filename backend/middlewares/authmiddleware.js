const jwt = require("jsonwebtoken");

// mendecode jwt tokennya
module.exports = (req, res, next) => {
  // check the headers
  if (req.headers["authorization"]) {
    const token = req.headers["authorization"].split(" ")[1];
    jwt.verify(token, process.env.SECRET, (err, decoded) => {
      if (err) {
        next(Error("Failed authenticate the token"));
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    next(Error("No token"));
  }
};
